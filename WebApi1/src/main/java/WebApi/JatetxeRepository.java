
package WebApi;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "Proiektua", path = "jatetxea")
public interface JatetxeRepository extends MongoRepository<Jatetxea, String> {

	List<Jatetxea> findByIdentificador(@Param("identificador") int identificador);

}
